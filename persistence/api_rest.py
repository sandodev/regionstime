import requests


class RestApi:

    def getAllRegions(self):
        """
        Get all API regions

        Returns a list with the regions
        """
        regions = []
        url = "https://restcountries-v1.p.rapidapi.com/all"
        headers = {
            'x-rapidapi-host': "restcountries-v1.p.rapidapi.com",
            "x-rapidapi-key": "e6068a7901mshd94952167d3351ap19663djsn91e0d86246da"
        }

        response = requests.get(url, headers=headers)

        if response.status_code == 200:
            data_json = response.json()

            for i in range(len(data_json)):
                if len(regions) == 0:
                    regions.append(data_json[i]['region'])

                if data_json[i]['region'] in regions:
                    continue

                regions.append(data_json[i]['region'])

            return regions
        else:
            print("Oops something is wrong, server status code: " +
                  response.status_code)
            return regions

    def getCountryByRegion(self, regions):
        """
        Get an API country for each existing region in a list of regions

        Parameters: list with regions
        
        Returns a list of dictionaries with one country by region
        """
        countries = []
        url = "https://restcountries.eu/rest/v2/all"
        response = requests.get(url)

        if response.status_code == 200:
            data_json = response.json()

            for i in range(len(data_json)):

                if data_json[i]['region'] in regions:
                    #countries.append({'region': data_json[i]['region'], 'name': data_json[i]['name']})

                    if len(countries) == 0:
                        countries.append(
                            {'region': data_json[i]['region'], 'name': data_json[i]['name']})

                    for j in range(len(countries)):
                        if data_json[i]['region'] in countries[j]['region']:
                            break
                        if j == len(countries)-1:
                            countries.append(
                                {'region': data_json[i]['region'], 'name': data_json[i]['name']})

            return countries
        else:
            print("Oops something is wrong, server status code: " +
                  response.status_code)
            return countries

    def getLanguageByCountry(self, countries):
        """
        Get the language of an API for each country in a list

        Parameters: list of dictionaries with one country by region
        
        Returns a list of dictionaries with the language of a country by region
        """
        languages = []
        url = "https://restcountries.eu/rest/v2/all"
        response = requests.get(url)

        if response.status_code == 200:
            data_json = response.json()

            for i in range(len(data_json)):

                for j in range(len(countries)):
                    if data_json[i]['name'] == countries[j]['name']:
                        languages.append(
                            {'region': data_json[i]['region'], 'name': data_json[i]['name'], 'language': data_json[i]['languages'][0]['name']})
                        break

            return languages
        else:
            print("Oops something is wrong, server status code: " +
                  response.status_code)
            return languages
