import sqlite3
from sqlite3 import Error


def connectDB():
    """
    Connect to a database in sqlite
    """
    try:
        conn = sqlite3.connect('persistence/RegionsTime.db')
        print("Connection wtih data base established")
    except Error:
        print(Error)

    return conn


def createTable(conn):
    """
    Create two tables in the database, one for the information 
    of the region and another for the detail of the time
    """
    obj_cursor = conn.cursor()

    obj_cursor.execute(
        "SELECT name FROM sqlite_master WHERE TYPE='table' AND name='regions_time' ")
    table = obj_cursor.fetchone()
    obj_cursor.execute(
        "SELECT name FROM sqlite_master WHERE TYPE='table' AND name='details_time' ")
    table2 = obj_cursor.fetchone()

    if (table == None) and (table2 == None):
        obj_cursor.execute(
            "CREATE TABLE regions_time(id integer PRIMARY KEY," +
            "region TEXT, " +
            "coutry TEXT, " +
            "language TEXT," +
            "time REAL)"
        )
        obj_cursor.execute(
            "CREATE TABLE details_time(total_time REAL, " +
            "avg_time REAL, " +
            "min_time REAL," +
            "max_time REAL)"
        )
    elif (table[0] == "regions_time") and (table2[0] == "details_time") :
        obj_cursor.execute("DROP TABLE regions_time")
        obj_cursor.execute("DROP TABLE details_time")
        obj_cursor.execute(
            "CREATE TABLE regions_time(id integer PRIMARY KEY," +
            "region TEXT, " +
            "coutry TEXT, " +
            "language TEXT," +
            "time REAL)"
        )
        obj_cursor.execute(
            "CREATE TABLE details_time(total_time REAL, " +
            "avg_time REAL, " +
            "min_time REAL," +
            "max_time REAL)"
        )

    conn.commit()


def fillTable(conn, data_full, times_full):
    """
    Fill the tables with the information obtained by the API
    """
    obj_cursor = conn.cursor()
    name = ""

    for i in range(len(data_full)):

        # Remove quotation marks( ' ) from the name
        name = data_full[i]["Country"].replace("'", " ")

        obj_cursor.execute(
            "INSERT INTO regions_time VALUES({},'{}','{}','{}',{})".format(i, 
                data_full[i]["Region"], 
                name, 
                data_full[i]["Language"], 
                data_full[i]["Time"])
        )

    obj_cursor.execute(
        "INSERT INTO details_time VALUES({},{},{},{})".format(
            times_full[0]["total_time"], 
            times_full[1]["avg_time"], 
            times_full[2]["min_time"], 
            times_full[3]["max_time"])
    )

    conn.commit()
