from persistence import connection
from timeit import default_timer
import json
import pandas as pd
from hashlib import sha1


def makeJson(data_full):
    """
    Create a json file with the information obtained by the API
    """
    file = open("view/data.json", "w", encoding="utf-8")
    data_json = json.dumps(data_full)
    file.write(data_json)
    file.close()


def makeDB(data_full, times_full):
    """
    Manage the database
    """
    conn = connection.connectDB()
    connection.createTable(conn)
    connection.fillTable(conn, data_full, times_full)
    conn.close()


def dataFrame(data):
    """
    Shows and processes the information obtained by the API in the console
    """
    data_full = []
    times_full = []
    language_encryp = ""
    total_time = 0.0
    avg_time = 0.0
    min_time = 0.0
    max_time = 0.0
    ini_time = 0.0
    end_time = 0.0

    for i in range(len(data)):
        ini_time = default_timer()
        language_encryp = sha1(data[i]['language'].encode('utf-8')).hexdigest()
        nameC = str(data[i]['name'].encode('utf-8'))
        data_full.append({
            'Region': data[i]['region'],
            'Country': nameC,
            'Language': language_encryp,
            'Time': (default_timer() - ini_time)}
        )

    print(pd.DataFrame(data_full))
    times = pd.DataFrame(data_full)
    total_time = times['Time'].sum()
    avg_time = times['Time'].mean()
    min_time = times['Time'].min()
    max_time = times['Time'].max()

    times_full.append({"total_time": total_time})
    times_full.append({"avg_time": avg_time})
    times_full.append({"min_time": min_time})
    times_full.append({"max_time": max_time})

    print("Total time:", total_time)
    print("Average time:", avg_time)
    print("Minimum time:", min_time)
    print("Maximum time:", max_time)

    makeJson(data_full)
    makeDB(data_full, times_full)
