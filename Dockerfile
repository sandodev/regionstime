FROM ubuntu:18.04

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

WORKDIR /RegionsTime

COPY . /RegionsTime

RUN pip3 --no-cache-dir install -r requirements.txt

CMD [ "python3", "main.py" ]