# RegionsTime

_Technical test project to apply for the position of Backend Python in Zinobe._

**The following application creates a table with information on regions and information generation times.**

---

## Technical requirements

|  Region | City Name |  Languaje | Time  |
|---|---|---|---|
|  Africa | Angola  |  AF4F4762F9BD3F0F4A10CAF5B6E63DC4CE543724 | 0.23 ms  |
|   |   |   |   |
|   |   |   |   |

Desarrolle una aplicacion en python que genere la tabla anterior teniendo las siguientes consideraciones:

1. De https://rapidapi.com/apilayernet/api/rest-countries-v1, obtenga todas las regiones existentes.
2. De https://restcountries.eu/ Obtenga un pais por region apartir de la region optenida del punto 1.
3. De https://restcountries.eu/ obtenga el nombre del idioma que habla el pais y encriptelo con SHA1
4. En la columna Time ponga el tiempo que tardo en armar la fila (debe ser automatico)
5. La tabla debe ser creada en un DataFrame con la libreria PANDAS
6. Con funciones de la libreria pandas muestre el tiempo total, el tiempo promedio, el tiempo minimo y el maximo que tardo en procesar toda las filas de la tabla.
7. Guarde el resultado en sqlite.
8. Genere un Json de la tabla creada y guardelo como data.json
9. La prueba debe ser entregada en un repositorio git.
10. No usar Framework
11. Usar Test Unitarios
12. Presenta un diseño de su solucion.

**Plus** 
13. Entregar Dockerfile y docker compose con la solucion, en la que se guarde la data en un docker en mongo ademas de sqlite local.

---

## Instalation

1. Initialize virtual enviroment 
2. Install the requirements.txt in your virtual enviroment

---

## Execution

### Locally

1. Execute main.py

**Output**

1. Generate the table view in console
2. Shows the results of the information generation time, in console
3. Generate database with two tables and the information displayed in console
4. Generates the data.json file with the information from the table seen in the console

### With Docker

1. Initialize your docker service
2. Download the repository
3. Create the docker image with the dockerfile file
4. Run a container with the created image

---

## Autor
**Camilo Andrés Sandoval Delgado**

Github: [SandoDev](https://github.com/SandoDev)