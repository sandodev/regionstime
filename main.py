import unittest
from persistence.api_rest import RestApi
from view import generate


class TestFunctions(unittest.TestCase):

    def test_regions_list(self):
        regions = ['Asia', 'Europe', 'Africa',
                   'Oceania', 'Americas', 'Polar', '' ]
        self.assertEqual(regions, RestApi.getAllRegions(self),"List with incorrect results")

    def test_make_json(self):
        data = ["hello"]
        self.assertTrue(generate.makeJson(data), "Does not return true after executing its process")


if __name__ == "__main__":
    regions = []
    countries = []
    languages = []

    # Unit tests
    # unittest.main()

    obj_api = RestApi()
    regions = obj_api.getAllRegions()

    obj2_api = RestApi()
    countries = obj2_api.getCountryByRegion(regions)

    obj3_api = RestApi()
    languages = obj3_api.getLanguageByCountry(countries)

    generate.dataFrame(languages)
